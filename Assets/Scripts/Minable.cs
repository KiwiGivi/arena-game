﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Minable : MonoBehaviour
{
    public float mineTime = 7f;
    private float currentTime = 0f;
    private int amountOres = 3;
    public string oreType = "green gem";
    public bool available = true;
    private bool mined = false;

    float currentDeadTime = 0f;
    float deadTime = 30f;
    int damage = 0;

    Player player;

    private void Update()
    {
        if (mined)
        {
            currentDeadTime += Time.deltaTime;

            if (currentDeadTime >= deadTime)
            {
                currentDeadTime = 0f;
                mined = false;
                damage = 0;
                foreach (Transform child in transform)
                {
                    child.gameObject.GetComponent<MeshRenderer>().enabled = true;
                }
                
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (mined) return;
        if (other.CompareTag("Detection"))
        {
            available = false;
            player = other.gameObject.transform.root.gameObject.GetComponent<Player>();
            if (player)
            {
                player.withinMiningRange = true;
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (mined) return;
        if (other.CompareTag("Detection"))
        {
            currentTime += Time.deltaTime;
            if(currentTime>=mineTime)
            {
                //player receives ore
                currentTime = 0f;
                other.gameObject.transform.root.gameObject.GetComponent<Player>().GetItem(oreType);
                damage++;
                if (damage >= amountOres)
                {
                    mined = true;
                    damage = 0;
                    foreach (Transform child in transform)
                    {
                        child.gameObject.GetComponent<MeshRenderer>().enabled = false;
                    }
                    player.stopSkill();
                    
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (mined) return;
        if (other.CompareTag("Detection"))
        {
            available = true;
            Player player = other.gameObject.transform.root.gameObject.GetComponent<Player>();
            if (player)
            {
                player.withinMiningRange = false;
            }

        }
    }
}
