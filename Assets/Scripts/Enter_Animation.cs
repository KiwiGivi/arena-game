﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Enter_Animation : MonoBehaviour
{
    public GameObject enterTextPrefab;
    private GameObject textObject;
    public string info;
    public Vector3 offset;
    public Vector3 enterPos;
    public Vector3 leavePos;
    bool inside = false;
    bool released = false;
    Player pl;
    // Start is called before the first frame update
    void Start()
    {
        leavePos = transform.position;
        enterPos = transform.Find("insidehouse").transform.position;
        Debug.Log(enterPos);
    }

    // Update is called once per frame
    void Update()
    {
        if (!released && Input.GetKeyUp(KeyCode.E))
        {
            released = true;
        }
        if (inside && released)
        {
            /*if(Input.GetKeyDown(KeyCode.E))
            {
                inside = false;
                GetBack();
            }*/
        }
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (enterTextPrefab)
        {
            if (other.CompareTag("Player"))
            {
                pl = other.gameObject.GetComponent<Player>();
                ShowEnterText();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (textObject)
        {
            if (other.CompareTag("Player"))
                RemoveEnterText();
        }
    }

    private void OnTriggerStay(Collider other)
    {

        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("GOING IN!");
            Enter();
            RemoveEnterText();
        }
    }

    private void ShowEnterText()
    {
        textObject = Instantiate(enterTextPrefab, transform.position + offset, transform.rotation, transform);
        textObject.GetComponent<TextMeshPro>().text = info;
    }

    private void RemoveEnterText()
    {
        Destroy(textObject);
    }
    private void Enter()
    {
        
        if(pl)
        {
            pl.SetPos(enterPos);
            inside = true;
            released = false;

            
        }
    }
    private void GetBack()
    {
        if (pl)
        {
            pl.SetPos(leavePos);
            
        }
    }
}
