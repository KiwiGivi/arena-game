﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
public class Manager : MonoBehaviour
{
    public string playerPrefab;
    public string m_skelektonPrefab = "Skelekton"; // PhotonNetwork.Instantiate("Actors/PlayerControlled/Player")

    public Transform[] spawnPoints;
    private void Start()
    {
        Spawn();
    }
    public void Spawn()
    {
        Transform spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
        PhotonNetwork.Instantiate(playerPrefab, spawnPoint.position, spawnPoint.rotation);

        // Spawn a skelekton
       // PhotonNetwork.Instantiate(m_skelektonPrefab, spawnPoint.position, spawnPoint.rotation);
        //Debug.Log("Spawning Skelekton");

    }
    public void SpawnSkele(Transform pos)
    {
        PhotonNetwork.Instantiate(m_skelektonPrefab, pos.position+2f*Vector3.forward, pos.rotation);
    }
}
