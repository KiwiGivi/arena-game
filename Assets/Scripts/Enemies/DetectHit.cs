﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectHit : MonoBehaviour
{
    private int damage = 12;
    public GameObject lastHit;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && other.gameObject != transform.root.gameObject)
        {
           
            Debug.Log("hit player");
            Health health = other.GetComponent<Health>();
            if (health)
            {
                if (transform.root.gameObject.CompareTag("Player"))
                {
                    int damageTemp = gameObject.transform.root.gameObject.GetComponent<Player>().strength + Mathf.RoundToInt(Random.Range(1, 5));
                    health.TakeDamage(damageTemp);
                }
                else health.TakeDamage(12);
            }
            

        }

        if (other.CompareTag("Enemy") && transform.root.gameObject.CompareTag("Player"))
        {
            Debug.Log("hit enemy");
            
                other.GetComponentInChildren<DetectHit>().lastHit = transform.root.gameObject;
            // Debug.Log("lasthit is now " + transform.root.gameObject.tag);
            //deactivate hitbox
            gameObject.transform.root.gameObject.GetComponent<Player>().hitboxCollider.enabled = false;
            gameObject.transform.root.gameObject.GetComponent<Player>().hitboxMesh.enabled = false;
            // other.GetComponent
            Health health = other.GetComponent<Health>();
            transform.root.gameObject.GetComponent<Player>().PlaySwordSound();
            if(health)
            {
                int damageTemp = gameObject.transform.root.gameObject.GetComponent<Player>().strength + Mathf.RoundToInt(Random.Range(1, 5));
                health.TakeDamage(damageTemp);
            }
        }
    }
}
