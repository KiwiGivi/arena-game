﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class Health : MonoBehaviourPunCallbacks
{
   
    private int currentHealth;
    [SerializeField]
    private int totalHealth;
    private float expReward = 10f;
    private int moneyReward = 3;

    public GameObject dmgTextPrefab;
    private GameObject dmgText;

    // Start is called before the first frame update
    void Start()
    {
        if (gameObject.CompareTag("Enemy"))
        {
            if (transform.localScale.x > 15f)
                totalHealth = (Mathf.RoundToInt(transform.localScale.x) * 100) / 3;
        }
        currentHealth = totalHealth;   
    }

    // Update is called once per frame
    public void TakeDamage(int dmg)
    {
        if (dmg <= 0) return;
        if (gameObject.CompareTag("Player") && gameObject.GetComponent<Player>().currentState == 4) return;
        currentHealth -= dmg;

        //instantiate dmg text
        if (gameObject.CompareTag("Enemy"))
        {
            if (dmgText) Destroy(dmgText);
            dmgText = Instantiate(dmgTextPrefab, transform.position+new Vector3(0,-1,0), transform.rotation);
            dmgText.transform.Rotate(0, 180f,0, Space.World);
            dmgText.GetComponent<TextMeshPro>().text = dmg.ToString();
            //dmgText.GetComponent<TextMeshPro>().fontSize = 2f;
           // dmgText.transform.localScale = new Vector3(0.005f, 0.005f, 0.005f);
            
            StartCoroutine(RemoveDmgText(0.6f));
        }
        if (currentHealth < 0) currentHealth = 0;
        
        if (gameObject.CompareTag("Player"))
        {
            
            gameObject.GetComponent<Player>().UpdateHealth(true);
            return;
        }

        CheckHealth();
    }

    IEnumerator RemoveDmgText(float time)
    {
        yield return new WaitForSeconds(time);
        Debug.Log("DTESPOUEYEYOEYO");
        Destroy(dmgText);
    }
    private void CheckHealth()
    {
        if (gameObject.CompareTag("Enemy"))
        {
            gameObject.GetComponentInChildren<Slider>().value = (float)currentHealth/(float)totalHealth;
            //transform.Find("Fill").gameObject.GetComponent<Image>().color = Color.Lerp(Color.red, Color.green, (float)currentHealth / (float)totalHealth);
            //gameObject.GetComponentInChildren<Image>().color = Color.Lerp(Color.red, Color.green, (float)currentHealth / (float)totalHealth);
            if (currentHealth <= 0)
            {
                Debug.Log(gameObject.GetComponentInChildren<DetectHit>().lastHit.tag);
                // Debug.Log(gameObject.GetComponentInChildren<DetectHit>().lastHit.transform.Find("Warrior").gameObject.tag);

                gameObject.GetComponentInChildren<DetectHit>().lastHit.GetComponent<Player>().SetExperience(expReward * Mathf.RoundToInt(transform.localScale.x / 15));
                gameObject.GetComponentInChildren<DetectHit>().lastHit.GetComponent<Player>().GetGold(moneyReward*Mathf.RoundToInt(transform.localScale.x/15));

                PhotonNetwork.Destroy(gameObject);
            }
        }
    }
    public int GetCurrentHealth()
    {
        
        return currentHealth;
    }
    public int GetMaxHealth()
    {
        
        return totalHealth;
    }

    public void Heal(int amount)
    {
        currentHealth += amount;
        if(currentHealth>totalHealth)
        {
            currentHealth = totalHealth;
        }
        if (gameObject.GetComponent<Player>())
        gameObject.GetComponent<Player>().UpdateHealth(true);
    }
}
