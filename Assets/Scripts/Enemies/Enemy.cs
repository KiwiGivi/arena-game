﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    // Refrences to the player, evt target
    public Transform _target;
    // maybe have a spawn point target that the skelekton moves back to
    // if the player runs away from him.

    protected float m_Lookradius = 10.0f;
    public NavMeshAgent m_agent;
    public StateMachine _stateMachine;
    public int id = 1;

    [SerializeField]
    protected static Animator anim;

    protected void FaceTarget()
    {
        Vector3 direction = (_target.position - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime);
    }

    protected void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, m_Lookradius);
    }
}
