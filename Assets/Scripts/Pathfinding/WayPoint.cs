﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour
{
    [SerializeField]
    public List<WayPoint> connections;

    // Used for AStar
    [SerializeField]
    public float _gCost = 0;
    [SerializeField]
    public float _hCost = 0;
    [SerializeField]
    public float _fCost = 0;

    public WayPoint _parent;

    [SerializeField]
    public int _index = -1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.8f);

        // Draw Connections
        Gizmos.color = Color.gray;
        foreach (var node in connections)
        {
            Debug.DrawLine(node.transform.position, transform.position, Color.red);
        }
    }
}
