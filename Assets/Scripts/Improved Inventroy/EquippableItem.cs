﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipmentType
{
    Helmet,
    Chest,
    Gloves,
    Boots,
    Weapon1,
    Weapon2,
    Accessory1,
    Accessory2,

}

[CreateAssetMenu]
public class EquippableItem : Item
{
    public int StrengthBonus;
    public int IntelligenceBonus;
    public int EnduranceBonus;
    public int ArmorBonus;

    [Space]
    public EquipmentType equipmentType;

}
