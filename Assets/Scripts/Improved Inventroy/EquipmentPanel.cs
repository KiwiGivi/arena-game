﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

public class EquipmentPanel : MonoBehaviour
{
    [SerializeField] Transform equipmentSlotsParent;

    [SerializeField] EquipmentSlot[]  equipmentSlots;

    public event Action<Item> OnItemRightCLickEvent;

    private void Start()
    {
        for (int i = 0; i < equipmentSlots.Length; i++)
        {
            equipmentSlots[i].OnRightClickEvent += OnItemRightCLickEvent;
        }
    }

    private void OnValidate()
    {
        equipmentSlots = equipmentSlotsParent.GetComponentsInChildren<EquipmentSlot>();
    }

    public bool AddItem(EquippableItem item, out EquippableItem previousItem)
    {
        for(int i= 0; i < equipmentSlots.Length; i++)
        {
            if(equipmentSlots[i].EquipmentType == item.equipmentType)
            {
                previousItem = (EquippableItem)equipmentSlots[i].Item;
                equipmentSlots[i].Item = item;

                // Try and add stat bouns of item.
                GameObject p = PlayerManager.m_instance.m_player;
                p.GetComponent<Player>().IncreaseStrength(item.StrengthBonus);
                p.GetComponent<Player>().IncreaseArmor(item.ArmorBonus);

                // myObject.GetComponent<MyScript>().MyFunction();
                return true;
            }
        }

        previousItem = null;
        return false;
    }

    public bool RemoveItem(EquippableItem item)
    {
        for (int i = 0; i < equipmentSlots.Length; i++)
        {
            if (equipmentSlots[i].Item == item)
            { 
                // Try and remove stat bouns of item.
                GameObject p = PlayerManager.m_instance.m_player;
                p.GetComponent<Player>().ReduceStrength(item.StrengthBonus);
                p.GetComponent<Player>().ReduceArmor(item.ArmorBonus);
                
                equipmentSlots[i].Item = null;
                return true;
            }
        }

        return false;
    }

}
