﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic
{
   public static float ExpForNextLevel(int currentLvl)
    {
        if (currentLvl == 0) return 0;
        return (currentLvl * currentLvl + currentLvl + 3) * 4;
    }
}
