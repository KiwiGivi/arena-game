﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    [SerializeField]
    protected IState _startingState;
    protected IState _currentState; // the state that is currently running
    protected IState _previousState;

    // we might have variable that stores the object that is the owner of the StateMachine
    [SerializeField]
    private List<IState> _availableStates; // stores the available states that the FSM can use
    //Dictionary<FSMStateType, AbstractState> _fsmStates;

    public void Awake()
    {
        _currentState = null;
    }

    // Constructor
    public StateMachine()
    {
        _currentState = null;
    }

    void Start()
    {
        if(_startingState != null)
        {
            ChangeState(_startingState);
        }
    }

    // Excute the currently atvie
    public void Update()
    {
        if(_currentState != null)
        {
            _currentState.StateUpdate();
        }
    }

    public void ChangeState(IState newState) // Change state
    {
        // If there is not state, we can't exit (nullptr excpetion)
        if(_currentState != null)
        {
            _previousState = _currentState;
            _currentState.StateExit();
        }

        _currentState = newState;
        newState.StateEnter();
    }

    // Note that calling previous two times in a row now will not really work
    public void PreviousState()
    {
        if(_currentState != null && _previousState != null)
        {
            _currentState.StateExit();
            _currentState = _previousState;
            _currentState.StateEnter();
        }
    }
}