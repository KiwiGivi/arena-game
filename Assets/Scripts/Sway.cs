﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sway : MonoBehaviour
{
    #region Variables

    public float intensity;
    public float smooth;
    public bool isMine;

    private Quaternion originRotation;


    #endregion

    #region MonoBehaviour


    private void Start()
    {
        originRotation = transform.localRotation;
    }
    private void Update()
    {
        UpdateSway(); 
    }

    #endregion

    #region Private Methods

    private void UpdateSway()
    {
        //controls
        float tempXMouse = Input.GetAxis("Mouse X");
        float tempYMouse = Input.GetAxis("Mouse Y");

        if(!isMine)
        {
            tempXMouse = 0;
            tempYMouse = 0;
        }

        //calculate target location
        Quaternion tempXAdjustment = Quaternion.AngleAxis(intensity * -tempXMouse, Vector3.up);
        Quaternion tempYAdjustment = Quaternion.AngleAxis(intensity * tempYMouse, Vector3.right);

        Quaternion targetRotation = tempXAdjustment * tempYAdjustment * originRotation;

        //rotate towards target location
        transform.localRotation = Quaternion.Lerp(transform.localRotation, targetRotation, Time.deltaTime * smooth);
    }

    #endregion
}
